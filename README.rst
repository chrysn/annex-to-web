This is an in-development web server that serves a (bare?) git-annex repository over HTTP.

It is currently in very early stages of development,
and it's not sure whether it's going anywhere.

The canonical web site of this project is
<https://codeberg.org/chrysn/annex-to-web/>.

Planned features
----------------

This is a web server that is run on a
(typically but not necessarily bare)
git-annex repository, and serves its content.
(Non-bare repositories are supported,
but their index and directory contents are ignored).

Regular git files (blobs) and directories (trees)
are served directly or as a directory index, respectively.

Files under git-annex control are served when available,
and (depending on availability or configuration)
either obtained from other remotes and served on the fly,
redirect to other remotes that are known to have it and to run a similar server,
and worst case serve a "try making any of those remotes available" message.

Git submodules are served if there is a known (configured, or implied in non-bare repositories) clone usable.
Symlinks are followed within the git repository or available submodules,
and upwards to the including module when pointing above the current module.

Usage goal
----------

When complete, this program should be able to serve several media directories from a gitolite server.
This server can be run on one (with some additional extension possibly several) repository on each copy of a gitolite server,
turning the distributed version control system into a distributed web server.

This should be useful both for "real servers" (things out on the public internet)
and for mobile devices can thus allow their applications to access git-annex
in a fetch-as-needed way without the round-trip through fuse
(or with the round-trip via a http mounter, but without the need for an additional "annex-to-fuse" implementation).

Envisioned applications are

* Making a workgroup-wide shared repository available to devices without git-annex installed.
  git-annex's nice "get from the cheapest / fastest source" property can be preserved by suitable resolution for the chosen name,
  letting devices inside the office LAN get files from the local file server,
  while the first outside access copies it over to the public-facing mirror on the server site,
  only making one trip through the narrow uplink of the office.

  This is somewhat similar to the `public repository use case`_ of git-annex,
  but allows for absent git-annex files to be handled in a lazy fashion (or to redirect to a different server).
  By working from a bare repository, it can be used right with a gitolite server.

  .. _`public repository use case`: https://git-annex.branchable.com/tips/setup_a_public_repository_on_a_web_site/

* An MPD player is running MPD on Android and has a large collection of audio files in its index.
  Not all of them fit the local disk, but any actually accessed files are fetched if there is network connectivity
  and stored until further notice.

* Serving photo collections to mobile device in analogy to the MPD example would be a nice addition,
  but for this'd need a way to manage generated thumbnails
  (and image viewers that use shipped thumbnails).

* A potential side use case is serving BitTorrent data, doubling as a web seed
  (provided `some file naming discrepancies are resolved`_).
  This could help bridge disjunct BitTorrent swarms over HTTP,
  as any other swarm's nodes could access an annex-to-web server via HTTP,
  and if it doesn't have the files at hand,
  it can use its own BitTorrent peers in its own swarm to obtain the files.

  .. _`some file name discrepancies are resolved`: https://git-annex.branchable.com/todo/bittorrent__58___support_offline_operation_and_verification/

Current status
--------------

Git repositories, both bare and non-bare, are served with files and directory listings,
as are git-annex files inside them.

Absent git-annex files are recognized,
but do not start a ``git annex get`` yet;
they do show "see other remotes" message with further links
and redirect the user to one of those servers by configured priorities.

Locked git-annex files are completely disregarded.
(And how would I recognize them?)

Submodules are traversed, but so far only in checkouts
(as bare repositories do not contain submodules in the same way,
and will require additional configuration).
The submodules need to have their core.worktree option removed --
that requires an explicit GIT_WORK_TREE variable when running git-annex (which annex-to-web provides);
see [the issue at git-annex](https://git-annex.branchable.com/bugs/creating_dot-git-as-symlink_workaround_drops_worktree_configuration_from_submodules/) for details.

AOB
---

The project is written in Rust and can be run using ``cargo run "[::1]:3000" ./some/repository.git``
to serve on port 3000 of the loopback address.
It aims for asynchronous execution,
but is occasionally sloppy there,
hoping for the async ecosystem to provide easy solutions
before the sloppiness becomes a problem here.
(For example, ``git annex`` is called blockingly pending `an async-std Command implementation`_,
and git reads are just assumed to be served from cache).

.. _`an async-std Command implemenmtation`: https://github.com/async-rs/async-std/issues/22

Code was written by chrysn <chrysn@fsfe.org> and is published under the terms of the GNU AGPL 3.0 or any later version.
