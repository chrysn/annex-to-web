/// This module does / should do all the nontrivial web UI components.
///
/// The functions' signatures describe the model for the views.
use horrorshow::{html, owned_html, Template, TemplateBuffer};

use crate::AppError;

use hyper::{http::StatusCode, Body, Response};

use std::pin::Pin;

pub fn render_listing<'a>(
    folder_name: Option<&'a str>,
    items: impl Iterator<Item = (Option<String>, String)> + 'a,
) -> horrorshow::FnRenderer<impl FnOnce(&mut TemplateBuffer<'_>) + 'a> {
    owned_html! {
        h1 : format_args!("Directory listing of {}", folder_name.unwrap_or("/"));
        ul {
            @ if folder_name.is_some() { li { a(href = "..") { : ".." } } }
            @ for (link, label) in items {
                li {
                    a(href = link) {
                        : label
                    }
                }
            }
        }
    }
}

#[derive(Debug, PartialOrd, Ord, PartialEq, Eq)]
pub struct WebWhereisItem {
    pub detail: WebWhereisItemDetail,
    // careful: sequence matters for Ord
    description: String,
}

impl WebWhereisItem {
    // or "impl Render"?
    pub fn format(&self, tmpl: &mut horrorshow::TemplateBuffer) {
        match &self.detail {
            WebWhereisItemDetail::NotFound => {
                tmpl << html! {
                    dt { : &self.description }
                }
            }
            WebWhereisItemDetail::Web(urls) => {
                tmpl << html! {
                    dt { : "On the web" }
                    @for url in urls.iter() {
                        dd { a(href=url) { : url } }
                    }
                }
            }
            WebWhereisItemDetail::Torrent(urls) => {
                tmpl << html! {
                    dt { : "Using BitTorrent" }
                    @for url in urls.iter() {
                        dd { a(href=url) { : url } }
                    }
                }
            }
            WebWhereisItemDetail::HasServer(url) => {
                tmpl << html! {
                    dt { : &self.description }
                    dd {
                        a(href=url) { : url }
                    }
                }
            }
        }
    }
}

#[derive(Debug, PartialOrd, Ord, PartialEq, Eq)]
pub enum WebWhereisItemDetail {
    HasServer(String),
    Web(Vec<String>),
    Torrent(Vec<String>),
    NotFound,
}

impl WebWhereisItemDetail {
    pub fn with(self, description: String) -> WebWhereisItem {
        WebWhereisItem {
            description,
            detail: self,
        }
    }

    pub fn get_link(&self) -> Option<&str> {
        match self {
            WebWhereisItemDetail::HasServer(s) => Some(s),
            WebWhereisItemDetail::Web(v) => v.get(0).map(|s| s.as_str()),
            _ => None,
        }
    }
}

pub fn render_multiplechoices<'a>(
    file_name: String,
    whereis: impl Iterator<Item = WebWhereisItem> + std::iter::ExactSizeIterator + 'a,
) -> horrorshow::FnRenderer<impl FnOnce(&mut TemplateBuffer<'_>) + 'a> {
    owned_html! {
        h1 : "File not found locally";
        p {
            : "The file ";
            tt : &file_name;
            : " is not available locally";
            : if whereis.len() == 0 {
                    " and has no other known locations either."
                } else {
                    "; you may find it in any of those repositories:"
                };
        }
        ul {
            @for r in whereis {
                |tmpl| r.format(tmpl)
            }
        }
    }
}

impl AppError {
    pub fn to_response(
        &self,
    ) -> Response<
        Pin<Box<dyn hyper::body::HttpBody<Data = hyper::body::Bytes, Error = hyper::Error> + Send>>,
    > {
        match self {
            // Could as well provide a link up
            AppError::NotFound => html_response(
                StatusCode::NOT_FOUND,
                "Not found",
                html! { : "No such file or directory"; },
            ),
            AppError::AddASlash(actual) => html_response(
                StatusCode::NOT_FOUND,
                "Not found",
                html! {
                    : "No such file (but a ";
                    a(href=actual) : "directory with a similar name";
                    : ")";
                },
            ),
            AppError::DropTheSlash(actual) => html_response(
                StatusCode::NOT_FOUND,
                "Not found",
                html! {
                    : "No such directory (but a ";
                    a(href=actual) : "file with a similar name";
                    : ")";
                },
            ),
            AppError::DenormalizedPath => html_response(
                StatusCode::BAD_REQUEST,
                "Bad request",
                html! { : "Please normalize your paths before requesting"; },
            ),
            AppError::IncompleteGit(s) => html_response(
                StatusCode::INTERNAL_SERVER_ERROR,
                "Internal server error",
                html! { : format_args!("Broken repository: {}", s); },
            ),
            AppError::NotImplemented(s) => html_response(
                StatusCode::INTERNAL_SERVER_ERROR,
                "Internal server error",
                html! { : format_args!("Implementation limit: {}", s); },
            ),
        }
    }
}

pub fn html_response(
    status: hyper::StatusCode,
    title: impl std::fmt::Display,
    r: impl horrorshow::RenderOnce,
) -> Response<
    Pin<Box<dyn hyper::body::HttpBody<Data = hyper::body::Bytes, Error = hyper::Error> + Send>>,
> {
    let body = html! {
        : horrorshow::helper::doctype::HTML;
        html {
            head {
                title : format_args!("{}", title);
                link(rel="impl-info", href="https://codeberg.org/chrysn/annex-to-web/");
            }
            body {
                : r ;
            }
        }
    };
    let body: Body = body.into_string().unwrap().into();
    let boxed: Pin<
        Box<dyn hyper::body::HttpBody<Data = hyper::body::Bytes, Error = hyper::Error> + Send>,
    > = Box::pin(body);
    Response::builder()
        .status(status)
        .header(hyper::header::CONTENT_TYPE, "text/html;charset=utf-8")
        .body(boxed)
        .unwrap()
}
