//! Application options as exposed on the command line
use std::collections::HashMap;
use structopt::StructOpt;

// The configuration parts relevant to the the server or the handler
//
// For simplicity, they are all rolled into a single struct -- the `bind` data is needlessly
// passed into the handlers, but that does no great harm.
//
// Literals in here are owned heap allocations to avoid the lifetime mess of passing them through
// make_svc & co.
/// Run a web server that serves a (possibly bare) git repository, and serves git-annex files on
/// demand, pulling them in or redirecting to other servers where the data is.
#[derive(Debug, StructOpt)]
pub struct ApplicationConfigurationOpts {
    /// IP socket address to bind to. Example values are `[::]:8000` or `127.0.0.1:3000`.
    pub bind: std::net::SocketAddr,
    /// Git repository to serve under the root path
    pub repopath: std::path::PathBuf,
    /// Do not attempt to run `git annex get` on absent files and serve requests for them as they come in
    #[structopt(long="--no-get", parse(from_flag=negate))]
    pub get: bool,
    /// Pairs of UUIDs and corresponding base URIs. When files are not present and unobtainable,
    /// the user will be redirected to a location where the file is, with decreasing prefernence
    /// indicated by the order of command line arguments.
    ///
    /// If redirect is disabled, those are used to serve hyperlinks on the "not available locally"
    /// error page.
    ///
    /// Example: --redirect-base add3536d-a60a-4532-be54-e622efe3caf8:http://bologna.annex.example.com
    ///
    /// This option is being phased out, as it is redundant with the information stored in httpalso
    /// remotes (see https://git-annex.branchable.com/special_remotes/httpalso/)
    #[structopt(long="redirect-base", parse(try_from_str=colonsplit))]
    pub redirect_bases: Vec<(String, String)>,
    /// Pairs of UUIDs and priorities in which the URIs should be used when redirecting
    ///
    /// Files unavailable locally but available will be redirected to, provided there is a suitable
    /// redirect base. The location with the highest priority that has the file will be picked.
    ///
    /// This sequence also affects sorting in the list of available remotes which is shown if no
    /// redirection is possible. Negative values only affect sorting and will never be redirected
    /// to; UUIDs not listed here explicitly have a default priority of -100.
    ///
    /// (Note that if the web remote 00000000-0000-0000-0000-000000000001 is assigned a priority
    /// and multiple web locations are available, any will be picked.)
    ///
    /// Example: --redirect-priority add3536d-a60a-4532-be54-e622efe3caf8:30 --redirect-priority
    ///     f031c019-a5af-4870-83e8-6bd8bba3ade8:10
    ///
    /// This option is being re-evaluated in the lgiht of the remote.<name>.annex-cost parameter on
    /// httpalso remotes, as it partially overlaps with it.
    #[structopt(long="redirect-priority", parse(try_from_str=colonsplit))]
    pub redirect_priorities: Vec<(String, i32)>,
}

impl ApplicationConfigurationOpts {
    /// Process the easy-to-parse vecs into easy-to-use hashmaps
    pub fn preprocess(mut self) -> ApplicationConfiguration {
        ApplicationConfiguration {
            bind: self.bind,
            repopath: self.repopath,
            get: self.get,
            redirect_bases: self.redirect_bases.drain(..).collect(),
            redirect_priorities: self.redirect_priorities.drain(..).collect(),
        }
    }
}

/// Application configuration derived by preprocessing from ApplicationConfigurationOpts (see their
/// for field descriptions; the differencs should be trivial)
///
/// The distinct type is necesary to avoid building large custom parsers for the options, see
/// discussion at https://github.com/TeXitoi/structopt/issues/79
#[derive(Clone, Debug)]
pub struct ApplicationConfiguration {
    pub bind: std::net::SocketAddr,
    pub repopath: std::path::PathBuf,
    pub get: bool,
    pub redirect_bases: HashMap<String, String>,
    pub redirect_priorities: HashMap<String, i32>,
}

fn negate(a: bool) -> bool {
    !a
}

fn colonsplit<A, B>(input: &str) -> Result<(A, B), &str>
where
    A: core::str::FromStr,
    B: core::str::FromStr,
{
    let mut iter = input.splitn(2, ':');
    Ok((
        iter.next()
            .expect("splitn returns at least one item")
            .parse()
            .map_err(|_| "First component not understood")?,
        iter.next()
            .ok_or("Pairs must be split by a colon (':') character")?
            .parse()
            .map_err(|_| "Second component not understood")?,
    ))
}
