use std::convert::TryInto;
use std::pin::Pin;
use {
    hyper::{
        // This function turns a closure which returns a future into an
        // implementation of the the Hyper `Service` trait, which is an
        // asynchronous function from a generic `Request` to a `Response`.
        service::{make_service_fn, service_fn},
        // Miscellaneous types from Hyper for working with HTTP.
        Body,
        Error,
        Request,
        Response,
        Server,
    },
    std::net::SocketAddr,
};

use git2::Repository;

mod asyncread_hyperbody;
mod gitannex;
mod html;
mod options;

use options::{ApplicationConfiguration, ApplicationConfigurationOpts};

/// The information for processing the later entity gatherable from a TreeEntry's file mode field
enum FileModeHint {
    File, // no matter whether executable or not
    Directory,
    Symlink,
    Gitlink,
}

impl core::convert::TryFrom<&git2::TreeEntry<'_>> for FileModeHint {
    type Error = ();

    fn try_from(source: &git2::TreeEntry) -> Result<Self, ()> {
        match source.filemode() {
            0o120000 => Ok(FileModeHint::Symlink),
            0o160000 => Ok(FileModeHint::Gitlink),
            0o040000 => Ok(FileModeHint::Directory),
            0o100644 | 0o100755 => Ok(FileModeHint::File),
            _ => Err(()),
        }
    }
}

fn entry_to_href_and_name(t: git2::TreeEntry<'_>) -> (Option<String>, String) {
    let mode: Option<FileModeHint> = (&t).try_into().ok();

    // we already need to allocate for a string here as percent_encode doesn't
    // impl Render...
    let mut link =
        percent_encoding::percent_encode(t.name_bytes(), percent_encoding::NON_ALPHANUMERIC)
            .collect::<String>();
    // so we can append right away as well
    let link = match (mode, t.kind()) {
        (Some(FileModeHint::Gitlink), _) | (_, Some(git2::ObjectType::Tree)) => {
            link.push('/');
            Some(link)
        }
        (_, Some(git2::ObjectType::Blob)) => {
            // Actually, on symlinks, we should look in and determine what it is -- or at least add
            // a slash if the link ends with a slash, as a short-cut and initial workaround
            Some(link)
        }
        // A tree entry in a directory listing should, by what's allowed inside git, really not
        // point to anything else than a Tree or a Blob -- but if it does (eg. because someone
        // pushed a crafted repository here), things should not fall apart. Erring out in a
        // different way would be an option, but "I don't recognize what this is so I can name it
        // but not put a link behind it" sounds fair enough as well.
        _ => None,
    };

    let label = String::from_utf8_lossy(t.name_bytes());
    // If we didn't need to convert anyway to insert any replacement characters, I'm sure there is
    // a way to make the iterator hand out &TreeEntry rather than TreeEntry (so that references
    // inside can be returned that are valid until the next invocation), but right now this is a
    // fast solution.
    let label = label.to_string();

    (link, label)
}

// PartialEq: Just for asserts on results
#[derive(PartialEq)]
enum AppError {
    /// No such URI at all
    NotFound,
    /// No such URI (but you may want to add a slash to your request)
    ///
    /// The string indicates a usable relative reference
    AddASlash(String),
    /// No such URI (but you may want to remove a slash from your request)
    ///
    /// The string indicates a usable relative reference
    DropTheSlash(String),
    DenormalizedPath,
    IncompleteGit(&'static str),
    NotImplemented(&'static str),
}
async fn serve_req_errormapping(
    req: Request<Body>,
    config: ApplicationConfiguration,
) -> Result<
    Response<
        Pin<Box<dyn hyper::body::HttpBody<Data = hyper::body::Bytes, Error = hyper::Error> + Send>>,
    >,
    hyper::Error,
> {
    Ok(serve_req(req, config)
        .await
        .unwrap_or_else(|e| e.to_response()))
}

trait Cursor<'a>: Sized {
    /// Move the cursor into an item (folder / file) identified by the component name.
    ///
    /// This will process all submodules or symlinks for that item before returning.
    fn walk_down(self, component: std::borrow::Cow<'a, str>) -> Result<Self, AppError>;
}

struct GitCursor<'a> {
    /// Indicates the repository currently traversed
    repo: Repository,
    /// Indicates the tree or blob currently focused at
    oid: git2::Oid,
    /// Indicate whether the OID points to a blob that is to be treated as a symlink
    mode: FileModeHint,
    /// Tracks `repo` with a working directory to call `git annex` in. This is always "the git
    /// repository" (in the sense of "the checkout that contains .git" in non-bare repos, and the
    /// bare repo (being the only thing there is) in bare repos), because that is where git-annex
    /// needs to be called from.
    ///
    /// This component is only relevant to git-annex
    repopath: std::path::PathBuf,
    /// Chain of path components inside the repository
    ///
    /// This component is only relevant for looking up submodules and for rewinding the tree
    /// traversal in case of up-pointing symlinks.
    path_in_repo: Vec<std::borrow::Cow<'a, str>>,
}

impl<'a> GitCursor<'a> {
    /// Create a Cursor from the git repository at a given path, starting with whichever tree HEAD
    /// currently points to
    fn from_repo_head(path: &std::path::Path) -> Result<Self, AppError> {
        let repo = open_repo(path)?;

        let head = repo
            .head()
            .map_err(|_| AppError::IncompleteGit("HEADless repository"))?
            .target()
            .ok_or_else(|| AppError::IncompleteGit("HEAD has no value"))?;
        let oid = repo
            .find_commit(head)
            .map_err(|_| AppError::IncompleteGit("No commit at HEAD"))?
            // Or maybe it's better to store the Object? Currently aiming for readability, hoping
            // performance does not suffer.
            .tree_id();
        Ok(GitCursor {
            repo,
            oid,
            mode: FileModeHint::Directory,
            repopath: path.to_path_buf(),
            path_in_repo: Vec::new(),
        })
    }

    /// Given a cursor position that may be on a submodule (later, also on a symlink), return a
    /// resolved version of it.
    ///
    /// This does not resolve the symlinks yet, as those eventually need distinguishing between
    /// git-annex and other symlinks.
    fn resolve_linkish(mut self) -> Result<Self, AppError> {
        // FIXME following symlinks would go here

        // I'd be tempted to make this one of my "more of an if" while loops, but the fact that
        // we're hard-setting the cursor to mode=Directory on entering a new repository because
        // a commit's tree is a Tree and not a TreeEntry would make that pointless -- or in short:
        // the root of a git repository can't be a submodule.
        if let FileModeHint::Gitlink = self.mode {
            // Creating a new cursor for the submodule
            //
            // Not changing cursor.repo -- that's a lie that later breaks the commit lookup.
            // We'd have to,
            // * look the path-so-far up in a configured list of submodules, or
            // * look the path-so-far up in .gitmodules by path, and come up with something from
            //   the url, or
            // * in a non-bare repo, we could also just open repo + path-so-far
            // self.repo = open_repo("/tmp/nonbarerepo2/submodules/some-submodule/")
            // self.repo = open_repo("/tmp/nonbarerepo2/.git/modules/submodules/some-submodule/")
            //     .map_err(|_| AppError::IncompleteGit("Error opening repository"))?;

            // FIXME: implement a find_submodule_by_path using iteration over submodules
            // Just joining the paths is OK here -- even if it's "../../etc", it will only open a
            // repo if that's already a submodule.
            if let Ok(Ok(sub)) = self
                .repo
                .find_submodule(&self.path_in_repo.join("/"))
                .map(|x| x.open())
            {
                self.repo = sub;
                self.repopath = std::path::PathBuf::from(
                    self.repo
                        .workdir()
                        .expect("A found submodule should be a non-bare repository on its own"),
                );
            }
            // FIXME: Detect that none of the discovery paths worked rather than just sticking with
            // he curren one
            self.oid = self
                .repo
                .find_commit(self.oid)
                .map_err(|_| {
                    AppError::IncompleteGit("Submodule commit not found in indicated repository")
                })?
                .tree_id();
            self.mode = FileModeHint::Directory;
            self.path_in_repo.clear();
        }

        Ok(self)
    }
}

impl<'a> Cursor<'a> for GitCursor<'a> {
    fn walk_down(mut self, component: std::borrow::Cow<'a, str>) -> Result<Self, AppError> {
        {
            let tree = self
                .repo
                .find_tree(self.oid)
                .map_err(|_| AppError::IncompleteGit("Subtree missing in sparse checkout"))?;

            let entry = tree.get_name(&component).ok_or(AppError::NotFound)?;

            self.mode = (&entry)
                .try_into()
                .map_err(|_| AppError::IncompleteGit("Unknown entry mode"))?;

            // Here would be the place to look at .gitattributes as well

            self.oid = entry.id();
        }

        self.path_in_repo.push(component);

        Ok(self.resolve_linkish()?)
    }
}

/// Provide an HTTP response to a request for a given git-annex key in the git-annex
/// repository that is indicated in the config.
///
/// repopath_is_bare is the only additional information needed on the repository (as it decides the
/// access method). In general this would probably work with git annex object stores outside a git
/// repository (think rsync remote) just as well; repopath_is_bare would be set to True then.
///
/// relative being passed in is merely an optimization -- it's the ".git/annex/objects/XX/YY/KK/KK"
/// part of the symlink from which the key was usually obtained.
///
/// The request is only used for its URI, which should be better managed when the URI templating
/// involved in redirect bases is understood better.
///
/// file_name is purely for display purposes; it is the file name indicated in the title on error
/// pages.
async fn serve_annexed<'a>(
    config: ApplicationConfiguration,
    repopath: &std::path::Path,
    repopath_is_bare: bool,
    relative: &str,
    key: &str,
    path_in_repo: Vec<std::borrow::Cow<'a, str>>,
    file_name: String,
) -> Result<
    Response<
        Pin<Box<dyn hyper::body::HttpBody<Data = hyper::body::Bytes, Error = hyper::Error> + Send>>,
    >,
    AppError,
> {
    let localpath;

    if repopath_is_bare {
        localpath = gitannex::hashpath(repopath, true, key);
    } else {
        localpath = repopath.join(relative);
        debug_assert!(localpath == gitannex::hashpath(repopath, repopath_is_bare, key));
    }

    let available = async_std::fs::File::open(localpath).await;

    if let Ok(file) = available {
        let length = file
            .metadata()
            .await
            .map_err(|_| AppError::IncompleteGit("Failed to stat existing file"))?
            .len();
        let boxed: Pin<
            Box<dyn hyper::body::HttpBody<Data = hyper::body::Bytes, Error = hyper::Error> + Send>,
        > = Box::pin(asyncread_hyperbody::AsyncReadHyperBody::new(file));
        return Ok(Response::builder()
            .header(hyper::header::CONTENT_LENGTH, format!("{}", length))
            .body(boxed)
            .unwrap());
    } else {
        let mut whereis = gitannex::whereis(repopath, key)
            .await
            .map_err(|_| AppError::IncompleteGit("Error invoking git-annex"))?;

        let mut whereis: Vec<_> = whereis
            // preferably something ...(self) -> impl Iterator<Item = T>
            .drain(..)
            .map(|x| {
                let priority = config
                    .redirect_priorities
                    .get(&x.uuid)
                    .copied()
                    .unwrap_or(-100);
                let item = if x.uuid == gitannex::WEB_UUID {
                    html::WebWhereisItemDetail::Web(x.urls)
                } else if x.uuid == gitannex::TORRENT_UUID {
                    html::WebWhereisItemDetail::Torrent(x.urls)
                } else if let Some(base) = config.redirect_bases.get(&x.uuid) {
                    // Treating them more like URI templates
                    //
                    // FIXME this sounds wasteful in performance compared to putting the
                    // percent_encode output right into a formatter, but let's only complain when
                    // benchmarked.
                    //
                    // While it'd be tempting to optimize this with something that keeps
                    // path_in_repo in its original shape in the background, such a stunt would be
                    // much bespoke string handling code, and fall flat if any stunts like "start
                    // in that subdirectory" are used (although it might be argued that
                    // path_in_repo is moot on those anyway because you can't have an URI template
                    // for such a remote, it'd be a set of subpath->url mappings at best -- but
                    // then, maybe path_in_repo is used for different things too, like going up
                    // through symlinks -- then, should one really go though the configured
                    // top-most item?).
                    let reencoded: Vec<std::borrow::Cow<str>> = path_in_repo.iter().map(|x| percent_encoding::utf8_percent_encode(x, percent_encoding::NON_ALPHANUMERIC).into()).collect();
                    html::WebWhereisItemDetail::HasServer(format!("{}/{}", base, reencoded.join("/")))
                } else {
                    html::WebWhereisItemDetail::NotFound
                }
                .with(x.description);
                (-priority, item)
            })
            // This makes the HTML's Fn type easier
            .collect();
        whereis.sort();

        if let Some(target) = whereis
            .iter()
            .filter(|(p, _)| *p <= 0)
            .filter_map(|(_, i)| i.detail.get_link())
            .next()
        {
            // That's a mouthful for "empty body" :-(
            let boxed: Pin<
                Box<
                    dyn hyper::body::HttpBody<Data = hyper::body::Bytes, Error = hyper::Error>
                        + Send,
                >,
            > = Box::pin(Body::empty());

            return Ok(Response::builder()
                .status(hyper::StatusCode::FOUND)
                .header(hyper::header::LOCATION, target)
                .body(boxed)
                .unwrap());
        }

        return Ok(html::html_response(
            hyper::StatusCode::MULTIPLE_CHOICES,
            "File not found locally",
            html::render_multiplechoices(file_name, whereis.drain(..).map(|(_p, r)| r)),
        ));
        // FIXME provide a magnet link to load from *all* web sources?
    }
}

/// Wrapper for Repository::open that passes the typical parameetes for this application, and wraps
/// the error type.
fn open_repo(repopath: &std::path::Path) -> Result<Repository, AppError> {
    // No ceiling needed as no search enabled, but open_ext needs typed input anyway.
    let no_ceiling: &[&str] = &[];
    Repository::open_ext(
        repopath,
        // NO_SEARCH: Pointing to somewhere inside a repository would fail as the trailing path
        // information would be discarded, so it doesn't help, and could surprising (in a security
        // relevant way) if no git repo is present where expected.
        git2::RepositoryOpenFlags::NO_SEARCH,
        no_ceiling,
    )
    .map_err(|_| AppError::IncompleteGit("Error opening repository"))
}

/// Given a path as produced by the .uri().path() of a Hyper request (the path component of a URI,
/// starting with a slash), produce an iterator over the percent-decoded path segments.
///
/// The segments are decoded into UTF-8 strings, and checked for following syntax-based
/// normalization (ie. not containing empty interior segments or dot segments).
///
/// The presence of a trailing slash (which is the only normalized way to get an empty segment) is
/// indicated by the boolean result.
fn request_path_to_segments(
    mut path: &str,
) -> (
    impl Iterator<Item = Result<std::borrow::Cow<'_, str>, AppError>>,
    bool,
) {
    // Prelude to path-normalization: Chopping off the trailing slash into
    // a boolean makes the later checks for whether the path is normalized much easier.
    let path_trailing_slash;
    {
        let mut splitlastslash = path.rsplitn(2, '/');
        if splitlastslash.next() == Some("") {
            path = splitlastslash
                .next()
                .expect("There is at least one slash in a hyper path");
            path_trailing_slash = true;
        } else {
            path_trailing_slash = false;
        }
    }

    // Not turning this into a std::path::Path becaus that'd introduce all the trouble of OsPath,
    // and between a bare git repo and a URI there need not be such confusion -- but decomposing
    // the URI's percent encoding makes the names more straight-forward to handle in an "arbitrary
    // strings as path components" setup that git largely is. (".", ".." and "" not being
    // permissible is a components is a limitation of URIs, not git).
    let mut path = path.split('/').map(|component| {
        percent_encoding::percent_decode_str(component)
            .decode_utf8()
            // Given we'd fail to use a [u8] further down, we'll use str right away
            //
            // In other places (esp. at the rendering side), those are correctly handled to the
            // extent possible.
            .map_err(|_| {
                AppError::NotImplemented("git2 does not implement get_name for non-UTF8 filenames")
            })
    });

    // Not explicitly documented in hyper, but practically required by the underlying protocol;
    // stripping off that leading slash.
    //
    // This best happens here after the split (as it consumes the one iteration element split
    // always produces) but before normalization (because the first element is indeed empty).
    let empty = path.next();
    assert!(empty == Some(Ok(std::borrow::Cow::Borrowed(""))));

    // Ensure path is syntax-normalized. This is primarily a precaution -- those paths' wouldn't be
    // practically usable (as browsers do syntax normalization before they GET), and if any
    // downstream component happens to be added later that does not walk the tree itself but passes
    // the opaque identifiers to something that'd put meaning to them (eg. the file system),
    // potential security issues are caught early here.
    let path = path.map(|component| {
        component.and_then(|x| {
            if x == "." || x == ".." || x == "" {
                Err(AppError::DenormalizedPath)
            } else {
                Ok(x)
            }
        })
    });

    (path, path_trailing_slash)
}

async fn serve_req(
    req: Request<Body>,
    config: ApplicationConfiguration,
) -> Result<
    Response<
        Pin<Box<dyn hyper::body::HttpBody<Data = hyper::body::Bytes, Error = hyper::Error> + Send>>,
    >,
    AppError,
> {
    let (mut path, path_trailing_slash) = request_path_to_segments(req.uri().path());

    // Skipping the topic of attrs for now as they're traditionally not considered in bare repos.
    // Consideration would need to be added later by manually interpreting .gitattributes files
    // found during walking.
    // Also, for submodule support, this would need to be checked ... when?
    //     let attrs = repo.get_attr(path, "foo", git2::AttrCheckFlags::FILE_THEN_INDEX);

    let mut cursor = GitCursor::from_repo_head(config.repopath.as_path())?;

    // Name that's used to display of an HTML directory listing or file information as a title;
    // none only for the root (could be removed if fancy displays were not desired).
    let mut item_name = None;

    while let Some(component) = path.next().transpose()? {
        item_name = Some(component.clone()); // Most of the time it's a Borrowed anyway

        // There is a new path component to follow, thus we assume we can walk whatever is there,
        // and all we can walk down is a tree -- if it's not a tree (or didn't become a tree after
        // symlink and submodule resolution in the last step), there is nothing below it.

        cursor = cursor.walk_down(component)?;
    }

    // Just indicating that we're not mutating this any more -- even though that is likely to
    // change when symlink traversal gets built in.
    let GitCursor {
        repo,
        oid,
        mode,
        repopath,
        path_in_repo
    } = cursor;

    if let FileModeHint::Symlink = mode {
        let reference = repo
            .find_object(oid, Some(git2::ObjectType::Blob))
            .map_err(|_| AppError::IncompleteGit("Symlink not backed by a blob object"))?
            .peel_to_blob()
            .map_err(|_| AppError::IncompleteGit("Symlink not backed by an actual blob"))?
            .content()
            // Cloned right away to not keep a git object open across an await point later on
            .to_vec();

        if reference.starts_with(b"/") {
            return Err(AppError::NotImplemented(
                "No resolution rules for absolute symlinks",
            ));
        }

        if let Some((relative, key)) = gitannex::is_annex_symlink(&reference) {
            let repo_is_bare = repo.is_bare();
            drop(path); // Needs to be told explicitly as the compiler can't see through the `impl` result that produced the path that it can be NLL'd to free req

            // Now we *do* need to clone this part of the path, as it may be a reference into the
            // req
            let file_name = item_name.expect("Files are always named").to_string();
            if path_trailing_slash {
                return Err(AppError::DropTheSlash(format!("../{}", file_name)));
            }
            return serve_annexed(
                config,
                &repopath,
                repo_is_bare,
                relative,
                key,
                path_in_repo,
                file_name,
            )
            .await;
        }

        if reference.starts_with(b"../") {
            return Err(AppError::NotImplemented(
                "No resolution rules for upwards symlinks yet",
            ));
        }

        return Err(AppError::NotImplemented(
            "Not even resolution rules for plain symlinks yet",
        ));
    }

    match (mode, path_trailing_slash) {
        (FileModeHint::Directory, false) => {
            Err(AppError::AddASlash(format!("{}/", item_name.unwrap())))
        }
        (FileModeHint::Directory, true) => {
            let tree = repo
                .find_tree(oid)
                .map_err(|_| AppError::IncompleteGit("Directory without tree content"))?;

            let item_name = item_name.as_ref().map(|x| x.as_ref());

            Ok(html::html_response(
                hyper::StatusCode::OK,
                format!("Directory listing of {}", item_name.unwrap_or("/")),
                html::render_listing(item_name, tree.iter().map(entry_to_href_and_name)),
            ))
        }
        (FileModeHint::File, true) => Err(AppError::DropTheSlash(format!(
            "../{}",
            item_name.unwrap().trim_end_matches('/')
        ))),
        (FileModeHint::File, false) => {
            let blob = repo
                .find_blob(oid)
                .map_err(|_| AppError::IncompleteGit("Regular file without blob content"))?;
            // Not sending a media type -- we don't know anyway, the browser's guess is as good
            // as ours
            // FIXME allow asynchronous access (but libgit2 doesn't work that way)
            let body: Body = hyper::body::Bytes::copy_from_slice(blob.content()).into();
            let boxed: Pin<
                Box<
                    dyn hyper::body::HttpBody<Data = hyper::body::Bytes, Error = hyper::Error>
                        + Send,
                >,
            > = Box::pin(body);
            Ok(Response::new(boxed))
        }

        (FileModeHint::Symlink, _) | (FileModeHint::Gitlink, _) => {
            unreachable!("Weeded out during iteration or served as annex files in previous steps")
        }
    }
}

async fn run_server(addr: SocketAddr, config: ApplicationConfiguration) {
    println!("Listening on http://{}, config {:?}", addr, config);

    let make_svc = make_service_fn(|_| {
        let config = config.clone();
        async {
            Ok::<_, Error>(service_fn(move |req| {
                let config = config.clone();
                async { serve_req_errormapping(req, config).await }
            }))
        }
    });

    Server::bind(&addr)
        .serve(make_svc)
        .await
        .expect("Failed to start server");
}

#[tokio::main]
async fn main() {
    use structopt::StructOpt;
    let args = ApplicationConfigurationOpts::from_args().preprocess();

    run_server(args.bind, args).await;
}
