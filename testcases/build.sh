#!/bin/sh
#
# Usage: ./build.sh some-path
#
# Creates a directory at some-path and creates the full set of test repositories in there

set -e

if [ -f "$1/is-test-directory" ]
then
	chmod u+w -R "$1/"
	rm -rf "$1/"
fi

mkdir "$1"
cd "$1"
BASE="${PWD}"

touch "${BASE}/is-test-directory"

cd "${BASE}"
git init secondlevelsub
cd secondlevelsub
git annex reinit affeaffe-0002-1234-1234-100000000001
git annex describe here secondlevelsub-full
echo "local2" > local2.txt
git add local2.txt
echo "annexed2" > annexed2.txt
git annex add annexed2.txt
git commit -m "Created test setup for second level submodule"

cd "${BASE}"
git init firstlevelsub
cd firstlevelsub
git annex reinit affeaffe-0001-1234-1234-100000000001
git annex describe here firstlevelsub-full
echo "local1" > local1.txt
git add local1.txt
echo "annexed1" > annexed1.txt
git annex add annexed1.txt
mkdir deep1
git submodule add ../secondlevelsub deep1/secondlevelsubmodule
git commit -m "Created test setup for first level submodule"

cd "${BASE}"
git init basictest-full
cd basictest-full
git annex reinit affeaffe-0000-1234-1234-100000000001
git annex describe here basictest-full
echo "This is a plain local file" > README.txt
git add README.txt
echo "This is text managed by git-annex" > annexed.txt
git annex add annexed.txt
echo "This text is forgotten immediately" > forgotten.txt
git annex add forgotten.txt
git annex drop --force forgotten.txt
mkdir subdirectory
git annex addurl http://ftp.debian.org/debian/ls-lR.gz || echo "Trouble adding from external, skipping web file"
cp -aL annexed.txt subdirectory/
git annex add subdirectory
git submodule add ../firstlevelsub firstlevelsubmodule
# This will have firstlevelsub checked out, but not its recursive childsecondlevelsub
git commit -m "Created basic test setup"

cd "${BASE}"
git clone --bare basictest-full basictest-bare.git
cd basictest-bare.git
git annex reinit affeaffe-0000-1234-1234-100000000002
git annex describe here basictest-bare

cd "${BASE}"
git clone --bare basictest-full basictest-bare-noannexinit.git

cd "${BASE}"
git clone basictest-full basictest-nonannexinit-recursive --recursive

cd "${BASE}"
git clone basictest-full basictest-clone-recursive --recursive
cd basictest-clone-recursive
git annex init
cd firstlevelsubmodule
git annex init
git config core.worktree ../../../firstlevelsubmodule # as it was before git-annex removed it, see https://git-annex.branchable.com/bugs/creating_dot-git-as-symlink_workaround_drops_worktree_configuration_from_submodules/
cd deep1
cd secondlevelsubmodule
git annex init
git config core.worktree ../../../../../../firstlevelsubmodule/deep1/secondlevelsubmodule
